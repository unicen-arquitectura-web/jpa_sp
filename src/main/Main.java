package main;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import modelo.Equipo;
import modelo.Jugador;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("MySQL");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		// Insertar equipo
		Equipo equipo = new Equipo("Boca Juniors");
		em.persist(equipo);
		
		// Insertar jugador
		Jugador jugador_1 = new Jugador(1, "Nelson Garrido", 1, 3, equipo);
		em.persist(jugador_1);
		
		Jugador jugador_2 = new Jugador(2, "Pedro Garcia", 1, 2, equipo);
		em.persist(jugador_2);
		
		// Obtener jugador
		Jugador jugador_select = em.find(Jugador.class, 1);
		System.out.println(jugador_select.toString());
		
		// Query JPQL
		@SuppressWarnings("unchecked")
		List<Jugador> jugadores = em.createQuery("SELECT j FROM Jugador j").getResultList();
		jugadores.forEach(j -> System.out.println(j.toString()));
		
		em.getTransaction().commit();
		em.close();
		emf.close();
	}

}
